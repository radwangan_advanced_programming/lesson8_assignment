﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace HWWAR
{
    public partial class Form1 : Form
    {
        private TcpClient _client;
        private NetworkStream _clientStream;
        private string _opponentCard = "";
        private string _myCard = "";
        public Form1()
        {
            InitializeComponent();
            button1.Enabled = false;
            //pictureBox1.Enabled = false;
            pictureBox2.Enabled = false;
            pictureBox3.Enabled = false;
            pictureBox4.Enabled = false;
            pictureBox5.Enabled = false;
            pictureBox6.Enabled = false;
            pictureBox7.Enabled = false;
            pictureBox8.Enabled = false;
            pictureBox9.Enabled = false;
            pictureBox10.Enabled = false;
            pictureBox11.Enabled = false;

            Thread t1 = new Thread(Connect);
            t1.Start();
        }

        private void Connect()
        {
            this._client = new TcpClient();

            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);

            try
            {
                this._client.Connect(serverEndPoint);
            }
            catch (Exception c)
            {
                MessageBox.Show("Socket Error!\n" + c.ToString());
                Environment.Exit(0);
            }
            this._clientStream = this._client.GetStream();
            while (_client.Connected)
            {
                try
                {

                    byte[] bufferIn = new byte[4];
                    int bytesRead = this._clientStream.Read(bufferIn, 0, 4);
                    string input = new ASCIIEncoding().GetString(bufferIn);

                    pictureBox1.Image = Properties.Resources.card_back_blue;
                    if (this._myCard != "")
                    {
                        GenerateCards();
                    }

                    if (input == "0000")
                    {
                        Invoke((MethodInvoker)delegate { EnableBottuns(); });//enable it

                    }
                    else if (input == "2000")
                    {
                        string st;
                        st = label1.Text + " " + label2.Text + "\n" + label3.Text + " " + label4.Text;
                        MessageBox.Show(st);
                        this._client.Close();
                        Environment.Exit(10);
                    }
                    else if (input[0] == '1')
                    {
                        string[] suits = new string[4] { "diamonds", "hearts", "spades", "clubs" };
                        int suitNum = 0;
                        string image;
                        if (input[3] == 'D')
                        {
                            suitNum = 0;
                        }
                        if (input[3] == 'H')
                        {
                            suitNum = 1;
                        }
                        if (input[3] == 'S')
                        {
                            suitNum = 2;
                        }
                        if (input[3] == 'C')
                        {
                            suitNum = 3;
                        }

                        int cardValue;
                        string tmp = ""+input[1] + input[2];
                        Int32.TryParse(tmp, out cardValue);
                        if (cardValue == 1)
                        {
                            tmp = "ace";
                        }
                        else if (cardValue == 11)
                        {
                            tmp = "jack";
                        }
                        else if (cardValue == 12)
                        {
                            tmp = "queen";
                        }
                        else if (cardValue == 13)
                        {
                            tmp = "king";
                        }
                        else
                        {
                            tmp = "_" + cardValue;
                        }


                        image = tmp +"_of_" + suits[suitNum];

                        this.pictureBox1.Image = (Image)Properties.Resources.ResourceManager.GetObject(image);
                        this._opponentCard = "" + input[1] + input[2];
                        if (this._myCard != "")
                        {
                            int a = 0;
                            int b = 0;
                            Int32.TryParse(this._myCard, out a);
                            Int32.TryParse(this._opponentCard, out b);
                            if (a > b)
                            { 
                                int score;
                                Int32.TryParse(label2.Text, out score);
                                score++;
                                label2.Text = score.ToString();

                                //Int32.TryParse(label4.Text, out score);
                                //score--;
                                //label4.Text = score.ToString();
                            }
                            else if( a < b)
                            { 
                                int score;
                                Int32.TryParse(label4.Text, out score);
                                score++;
                                label4.Text = score.ToString();

                                //Int32.TryParse(label2.Text, out score);
                                //score--;
                                //label2.Text = score.ToString();
                            }
                            // restoring everything to the start
                            this._opponentCard = "";
                            this._myCard = "";
                        }

                    }
                }
                catch (Exception e)
                {

                }
            }
            string s;
            s = label1.Text + " " + label2.Text + "\n" + label3.Text + " " + label4.Text;
            MessageBox.Show(s);
            Environment.Exit(10);
        }

        private void EnableBottuns()
            {
            button1.Enabled = true;
            pictureBox2.Enabled = true;
            pictureBox3.Enabled = true;
            pictureBox4.Enabled = true;
            pictureBox5.Enabled = true;
            pictureBox6.Enabled = true;
            pictureBox7.Enabled = true;
            pictureBox8.Enabled = true;
            pictureBox9.Enabled = true;
            pictureBox10.Enabled = true;
            pictureBox11.Enabled = true;
            // enable cards
            GenerateCards();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes("2000");
            this._clientStream.Write(buffer, 0, 4);
            this._clientStream.Flush();

            this._client.Close();
            Environment.Exit(0);
        }

        private void GenerateCards()
        {

            pictureBox2.Image = Properties.Resources.card_back_red;
            pictureBox3.Image = Properties.Resources.card_back_red;
            pictureBox4.Image = Properties.Resources.card_back_red;
            pictureBox5.Image = Properties.Resources.card_back_red;
            pictureBox6.Image = Properties.Resources.card_back_red;
            pictureBox7.Image = Properties.Resources.card_back_red;
            pictureBox8.Image = Properties.Resources.card_back_red;
            pictureBox9.Image = Properties.Resources.card_back_red;
            pictureBox10.Image = Properties.Resources.card_back_red;
            pictureBox11.Image = Properties.Resources.card_back_red;


        }

        private void RandomCard(int numImage)
        {
            if (this._opponentCard == "")
            {
                pictureBox1.Image = Properties.Resources.card_back_blue;
            }
            GenerateCards();

            Random rnd = new Random();
            string[] suits = new string[4] { "diamonds", "hearts", "spades", "clubs" };
            int cardValue = rnd.Next(1, 14);
            int cardsuit = rnd.Next(0, 4);
            string image;

            if (cardValue == 1)
            {
                image = "ace";
            }
            else if (cardValue == 11)
            {
                image = "jack";
            }
            else if (cardValue == 12)
            {
                image = "queen";
            }
            else if (cardValue == 13)
            {
                image = "king";
            }
            else
            {
                image = "_" + cardValue;
            }
            image = image + "_of_" + suits[cardsuit];
            object obj = Properties.Resources.ResourceManager.GetObject(image);
            switch (numImage)
            {
                case 2:
                    pictureBox2.Image = (Image)obj;
                    break;
                case 3:
                    pictureBox3.Image = (Image)obj;
                    break;
                case 4:
                    pictureBox4.Image = (Image)obj;
                    break;
                case 5:
                    pictureBox5.Image = (Image)obj;
                    break;
                case 6:
                    pictureBox6.Image = (Image)obj;
                    break;
                case 7:
                    pictureBox7.Image = (Image)obj;
                    break;
                case 8:
                    pictureBox8.Image = (Image)obj;
                    break;
                case 9:
                    pictureBox9.Image = (Image)obj;
                    break;
                case 10:
                    pictureBox10.Image = (Image)obj;
                    break;
                case 11:
                    pictureBox11.Image = (Image)obj;
                    break;
            }
            string stringCardValue;
            if ( cardValue < 10)
            {
                stringCardValue = "0" + cardValue;
            }
            else
            {
                stringCardValue = ""+cardValue;
            }
            byte[] buffer = new ASCIIEncoding().GetBytes("1"+ stringCardValue + Char.ToUpper(suits[cardsuit][0]));
            this._clientStream.Write(buffer, 0, 4);
            this._clientStream.Flush();

            this._myCard = stringCardValue;

            if (this._opponentCard != "")
            {
                int a = 0;
                int b = 0;
                Int32.TryParse(this._myCard, out a);
                Int32.TryParse(this._opponentCard, out b);
                if (a > b)
                { 
                    int score;
                    Int32.TryParse(label2.Text, out score);
                    score++;
                    label2.Text = score.ToString();

                    //Int32.TryParse(label4.Text, out score);
                    //score--;
                    //label4.Text = score.ToString();
                }
                else if (a < b)
                { 
                    int score;
                    Int32.TryParse(label4.Text, out score);
                    score++;
                    label4.Text = score.ToString();

                    //Int32.TryParse(label2.Text, out score);
                    //score--;
                    //label2.Text = score.ToString();
                }
                // restoring everything to the start
                this._opponentCard = "";
                this._myCard = "";
                //pictureBox1.Image = Properties.Resources.card_back_blue;
                //GenerateCards();

            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            RandomCard(2);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            RandomCard(3);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            RandomCard(4);
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            RandomCard(5);
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            RandomCard(6);
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            RandomCard(7);
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            RandomCard(8);
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            RandomCard(9);
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            RandomCard(10);
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            RandomCard(11);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            string st;
            st = label1.Text + " " + label2.Text + "\n" + label3.Text + " " + label4.Text;
            MessageBox.Show(st);
            base.OnFormClosing(e);
            byte[] buffer = new ASCIIEncoding().GetBytes("2000");
            this._clientStream.Write(buffer, 0, 4);
            this._clientStream.Flush();
            this._client.Close();
            Environment.Exit(10);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string st;
            st = label1.Text + " " + label2.Text + "\n" + label3.Text + " " + label4.Text;
            MessageBox.Show(st);
            byte[] buffer = new ASCIIEncoding().GetBytes("2000");
            this._clientStream.Write(buffer, 0, 4);
            this._clientStream.Flush();
            this._client.Close();
            Environment.Exit(10);

        }
    }
}
